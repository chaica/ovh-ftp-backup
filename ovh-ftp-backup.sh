#!/bin/bash
#
# Backing up the OVH server, checking the archive and sending it through FTP
#

# Copyright © 2013 Carl Chenet <chaica@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

BACKUPHOME=/home/backups
FTPUSER=ftpbackupser
FTPPASS=S0m3Th1nGH4rD
FTPSERVER=ftp.server.com
SERVER=youservername
MYSQLUSER=backup
MYSQLPASS=S0m4Th1nGTr1cKy
PKGLIST=$BACKUPHOME/installed-packages
DBDUMP=$BACKUPHOME/mysqldumpall.sql
ARCHIVE=$BACKUPHOME/$SERVER-backup-`date +%d-%m-%Y`.tar.bz2
ADMINEMAIL=your.email@domain.com

# create BACKUPHOME if not exists
mkdir -p $BACKUPHOME
# get installed Debian packages
dpkg --get-selections| awk -F' ' '{print $1}' > $PKGLIST
if [ $RETVAL != 0 ];then	
	echo "Issue while using dpkg --get-selections of $SERVER on $FTPSERVER" | mail -s "Issue while uploading $ARCHIVE" $ADMINEMAIL
fi
# get mysql database
mysqldump -u$MYSQLUSER -p$MYSQLPASS --all-databases > $DBDUMP
if [ $RETVAL != 0 ];then	
	echo "Issue while performing mysqldump of $SERVER on $FTPSERVER" | mail -s "Issue while uploading $ARCHIVE" $ADMINEMAIL
fi
# get directories and files
tar --preserve-permissions --preserve-order -j -c -f $ARCHIVE /etc /var /home /opt /usr/local/bin $DBDUMP \
	--exclude=/var/lib/mysql/data \
	--exclude=$BACKUPHOME/$SERVER-backup* \
	--exclude=/var/cache/apt/archives > /dev/null 2>&1
# remove one-month-old archive on the FTP
lftp -e "rm -f $SERVER-backup-`date -d "-10 day" +%d-%m-%Y`.tar.bz2;exit" -u $FTPUSER,$FTPPASS $FTPSERVER > /dev/null 2>&1
if [ $RETVAL != 0 ];then	
	echo "Issue while removing the old archive of $SERVER on $FTPSERVER" | mail -s "Issue while uploading $ARCHIVE" $ADMINEMAIL
fi
# send the archive to ftp account
lftp -e "mput $ARCHIVE;exit" -u $FTPUSER,$FTPPASS $FTPSERVER > /dev/null 2>&1
RETVAL=$?
if [ $RETVAL != 0 ];then	
	echo "Issue while uploading the archive from $SERVER to $FTPSERVER" | mail -s "Issue while uploading $ARCHIVE" $ADMINEMAIL
fi
# remove the local backups
rm -f $PKGLIST $DBDUMP $ARCHIVE
exit 0
